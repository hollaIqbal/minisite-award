import Head from "next/head";
import type {AppProps} from 'next/app'
import '../styles/globals.css'

function MyApp({Component, pageProps}: AppProps) {

    return (<>
        <Head>
            <title>Daftar pemenang top 100 kantor hukum terbaik 2022 versi Hukumonline berdasarkan 11 kategori terpilih.</title>
            <link rel="shortcut icon" href="https://www.hukumonline.com/static/images/favicon.ico" />
            <link rel="icon" href="https://www.hukumonline.com/static/images/favicon.ico" />
            <link rel="preconnect" href="https://fonts.googleapis.com"/>
            <link rel="preconnect" href="https://fonts.gstatic.com" />
            <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap"
                        rel="stylesheet"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        </Head>
        <Component {...pageProps} />
    </>)
}

export default MyApp
