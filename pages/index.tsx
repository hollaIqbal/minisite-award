
import Header from "../components/header";
import JumboTron from "../components/jumbotron"
import Desc from "../components/desc"
import Insight from "../components/insight"
import Ranking from "../components/ranking";
import Hline from "../components/hLine";
import Award from "../components/award";
import Footer from "../components/Footer";

export function Index() {

    return (
        <>
            <div>
                <Header/>
                <JumboTron/>
                <Desc/>
                <Insight/>
                <Hline/>
                <Ranking/>
                <Award/>
                <Footer/>
            </div>
        </>
    );
}

export default Index;
