const { join } = require('path');
const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  content: [
    join(__dirname, '/pages/**/*.{js,ts,jsx,tsx}'),
    join(__dirname, '/components/**/*.{js,ts,jsx,tsx}'),
  ],
  theme: {
    extend: {
      fontFamily: {
        nunito: ['Nunito Sans', 'sans-serif'],
        inter: ['Inter', 'sans-serif'],
      },
      screens: {
        desktop: '1366px',
        tablet: '768px',
        xs: '360px',
        ...defaultTheme.screens,
      },
      colors: {
        primary: {
          DEFAULT: '#003366',
          100: '#E0ECF9',
          200: '#89BFF4',
          300: '#2877C5',
          400: '#0052A3',
          500: '#05213E',
        },
        secondary: {
          DEFAULT: '#F8CA0F',
        },
        neutral: {
          DEFAULT: '#ffffff',
          50: '#F5F7F9',
          75: '#CFD9E2',
          100: '#A8B8C7',
          200: '#8398AB',
          300: '#607A93',
          400: '#2B4156',
          500: '#162533',
        },
      },
      fontSize: {
        'header-1': ['42px', {
          lineHeight: '50px',
          letterSpacing: '0.015em',
        }],
        'header-1-xs': ['25px', {
          lineHeight: '32px',
          letterSpacing: '0.015em',
        }],
        'header-2': ['35px', {
          lineHeight: '46px',
          letterSpacing: '0.005em',
        }],
        'header-2-xs': ['22px', {
          lineHeight: '30px',
        }],
        'header-3': ['29px', {
          lineHeight: '40px',
        }],
        'header-3-xs': ['20px', {
          lineHeight: '28px',
        }],
        'title': ['24px', {
          lineHeight: '36px',
        }],
        'title-xs': ['18px', {
          lineHeight: '28px',
        }],
        'subtitle': ['20px', {
          lineHeight: '30px',
          letterSpacing: '0.015em',
        }],
        'subtitle-xs': ['16px', {
          lineHeight: '26px',
        }],
        'body-1': ['18px', {
          lineHeight: '28px',
          letterSpacing: '0.005em',
        }],
        'body-1-xs': ['16px', {
          lineHeight: '26px',
        }],
        'body-2': ['16px', {
          lineHeight: '26px',
          letterSpacing: '0.0025em',
        }],
        'body-2-xs': ['14px', {
          lineHeight: '24px',
        }],
        'body-3': ['14px', {
          lineHeight: '22px',
          letterSpacing: '0.0013em',
        }],
        'body-3-xs': ['12px', {
          lineHeight: '20px',
          letterSpacing: '0.004em',
        }],
        'caption': ['12px', {
          lineHeight: '18px',
          letterSpacing: '0.015em',
        }],
        'caption-xs': ['10px', {
          lineHeight: '16px',
          letterSpacing: '0.004em',
        }],
        'small': ['10px', {
          lineHeight: '16px',
          letterSpacing: '0.05em',
        }],
        'small-xs': ['9px', {
          lineHeight: '14px',
          letterSpacing: '0.05em',
        }],
      },
      aspectRatio: {
        '4/3': '4 / 3',
      },
      boxShadow: {
        primary: '0px 8px 24px rgba(234, 124, 105, 0.32)',
        'inverse-top': '4px 4px 0 #f6f6f6',
        'inverse-bottom': '4px -4px 0 #f6f6f6',
      },
    },
  },
  plugins: [],
};
