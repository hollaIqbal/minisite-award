import styled from "styled-components";
import tw from "twin.macro";
import {ArrowRight} from "phosphor-react";
const contents=[
    {"title":"Top 50 Largest & Midsize Full Service Law Firm 2022", "link":"https://drive.google.com/file/d/1pgRI1XF1PcrlfPCfpwA8Mu5yQ4XA0nfq/preview","article":"https://www.hukumonline.com/berita/a/melihat-deretan-law-firm-terbesar-kategori-full-service-2022-lt6315807a6a737/"},
    {"title":"Top 10 Largest Litigation Practice Law Firm 2022", "link":"https://drive.google.com/file/d/1FUK23X-v1SWtV1rWHFrs3QQPuR7ZcgIw/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-kantor-hukum-terbesar-kategori-litigasi-2022-lt6315a4859c5eb/"},
    {"title":"Top 10 Largest Non-Litigation Practice Law Firm 2022", "link":"https://drive.google.com/file/d/1IyogZNbA9UppmCBpanppq7cVjfC6IT9S/preview","article":"https://www.hukumonline.com/berita/a/simak-deretan-law-firm-terbesar-kategori-non-litigasi-2022-lt631637fa18365/"},
    {"title":"Midsize Full Service <br/> Law Firm 2022", "link":"https://drive.google.com/file/d/1qSwo9bLd8pUcHGUtkHKfFMCiEZg-cDRM/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-pemenang-rising-star-law-firm-2022-lt63163c5d4a178/"},
    {"title":"Midsize Litigation PracticeLaw Firm 2022", "link":"https://drive.google.com/file/d/1DuzWlbyLzIpQC3S956RhRBJRv5e5gy3g/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-kantor-hukum-terbesar-kategori-litigasi-2022-lt6315a4859c5eb/?page=2"},
    {"title":"Midsize Non-Litigation Practice Law Firm 2022", "link":"https://drive.google.com/file/d/1TO0SKwajBZiaWVa7pRH0utNGUnpyXnKI/preview","article":"https://www.hukumonline.com/berita/a/simak-deretan-law-firm-terbesar-kategori-non-litigasi-2022-lt631637fa18365/"},
    {"title":"Rising Star Full Service Law Firm 2022", "link":"https://drive.google.com/file/d/1Aum9nlZP98f7iR4zEpY7ehmMeY1x_WRv/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-pemenang-rising-star-law-firm-2022-lt63163c5d4a178/"},
    {"title":"Rising Star Litigation Practice Law Firm 2022", "link":"https://drive.google.com/file/d/1u0f54mMVczXfh-SdJfr-TGanf32sVAP4/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-pemenang-rising-star-law-firm-2022-lt63163c5d4a178/?page=2"},
    {"title":"Rising Star Non Litigation Practice Law Firm 2022", "link":"https://drive.google.com/file/d/1hbH2pJynnHqdGpxbCybUqg8og7TB2d-W/preview","article":"https://www.hukumonline.com/berita/a/ini-jajaran-pemenang-rising-star-law-firm-2022-lt63163c5d4a178/?page=2"},
    {"title":"Most Female Partners Law Firm 2022", "link":"https://drive.google.com/file/d/1krkZi80Qcb-ADOn5_p2L8FkFeGtzLt1t/preview","article":"https://www.hukumonline.com/berita/a/melihat-deretan-law-firm-terbanyak-miliki-partner-wanita-2022-lt63163f9432fc2/"}]

const Wrapper = styled.div.attrs({
    className: 'max-w-5xl mx-auto flex flex-col  md:py-20 py-10',
})``
const WrapperHeader = styled.div.attrs({
    className: 'max-w-5xl mx-auto flex flex-col  py-4 text-center',
})`
  & {
    h2 {
      ${tw`font-nunito text-header-2-xs md:text-header-3 font-extrabold text-neutral-500`}
    }

    p {
      ${tw`font-inter text-body-1-xs md:text-body-1 text-neutral-200 text-center py-4`}
    }
  }
`
const WrapperHighlight = styled.div.attrs({
    className: ' py-4 px-5',
})``
const WrapperItem = styled.div.attrs({
    className: 'grid grid-cols-1 md:grid-cols-2 md:gap-2 md:px-0 px-5 py-4',
})``
const Item = styled.div.attrs({
    className: 'flex flex-col border-b last:border-b-0 md:border-b-0 py-10 md:pb-0',
})`& {
  img {
    ${tw`rounded`}
  }

  h3 {
    ${tw`font-nunito text-subtitle-xs md:text-subtitle font-semibold text-neutral-300 py-5 text-center `}
  }
  div.item{
    ${tw`flex justify-center md:pt-10 py-5`}
  }
  a{
    ${tw` font-nunito bg-primary py-2 px-4 md:py-3 md:px-6 rounded text-body-3-xs md:text-body-3 font-bold text-neutral-50`}
  }

}`

const Ranking = (props) => {
    return (<Wrapper id={"ranking"}>
            <WrapperHeader>
                <h2>THE RANKINGS</h2>
            </WrapperHeader>
        <WrapperHighlight>
            <div className=" border-b md:border-b-0 pb-10">
                <h3 className={"font-nunito text-subtitle-xs md:text-subtitle font-semibold text-neutral-300 py-5 text-center"}>Top 100 Indonesian Law Firms 2022</h3>
                <iframe src="https://drive.google.com/file/d/1EuRqvvyGERWPZ5Xbzo8TLkDlJDBPTT5N/preview" className={"aspect-4/3 w-full"}  allow="autoplay"></iframe>
                <div className={"flex justify-center pt-10"}>
                    <a href={"https://www.hukumonline.com/berita/a/ini-dia-deretan-law-firm-terbesar-di-indonesia-2022-lt63156a4b486cd/?page=1"} target={"_blank"} rel={"noreferrer"} className={" font-nunito bg-primary py-2 px-4 md:py-3 md:px-6 rounded text-body-3-xs md:text-body-3 font-bold text-neutral-50"}>
                        Baca Artikel
                        <ArrowRight  className={"ml-2 inline-block"} size={18} weight="bold" />
                    </a>
                </div>
            </div>
        </WrapperHighlight>
            <WrapperItem>
                {contents && contents.map((item,index)=>(
                    <Item key={`ranking-${index}`}>
                            <h3 className={""} dangerouslySetInnerHTML={{__html: item.title}}/>
                        <iframe src={item.link} className={"w-full aspect-square md:aspect-video"}  allow="autoplay"></iframe>
                        <div className={'item'}>
                            <a href={item.article} target={"_blank"}  rel="noreferrer" >
                                Baca Artikel
                                <ArrowRight  className={"ml-2 inline-block"} size={18} weight="bold" />
                            </a>
                        </div>
                    </Item>
                ))}

            </WrapperItem>
        </Wrapper>);
};
export default Ranking