import styled from "styled-components";
import tw from "twin.macro";
import Image from "next/image";
import styles from "../styles/award.module.css";
const contents=[
    {
        "title":"Ini Dia Deretan Law Firm Terbesar di Indonesia 2022",
        "desc":"Selama lima tahun berturut-turut kantor hukum Assegaf Hamzah & Partners (AHP) menduduki posisi pertama sebagai juara bertahan pada kategori “Top 100 Indonesian Law Firms 2022”.",
        "link":"https://www.hukumonline.com/berita/a/ini-dia-deretan-law-firm-terbesar-di-indonesia-2022-lt63156a4b486cd/?page=1",
        "image":"https://images.hukumonline.com/frontend/lt63156a4b486cd/medium_lt63156d764eb15.jpg"
    },
    {
        "title":"Melihat Deretan Law Firm Terbesar Kategori Full Service 2022",
        "desc":"Assegaf Hamzah & Partners (AHP) menjadi juara satu dari 50 kantor hukum yang masuk dalam nominasi kategori law firm besar full service dengan total memiliki 149 fee earners. Jumlah ini meningkat sebanyak 4 fee earners dari tahun 2021 lalu.",
        "link":"https://www.hukumonline.com/berita/a/melihat-deretan-law-firm-terbesar-kategori-full-service-2022-lt6315807a6a737/",
        "image":"https://images.hukumonline.com/frontend/lt6315807a6a737/medium_lt6315836acad9a.jpg"
    },
    {
        "title":"Ini Jajaran Kantor Hukum Terbesar Kategori Litigasi 2022",
        "desc":"SIP Law Firm kembali menjuarai posisi pertama dengan 67 fee earners. Jumlah earners di tahun 2022 meningkat sebanyak 15 fee earners bila dibandingkan survei kantor hukum tahun 2021.",
        "link":"https://www.hukumonline.com/berita/a/ini-jajaran-kantor-hukum-terbesar-kategori-litigasi-2022-lt6315a4859c5eb/",
        "image":"https://images.hukumonline.com/frontend/lt6315a4859c5eb/medium_lt6315a56c16290.jpg"
    },
    {
        "title":"Simak! Deretan Law Firm Terbesar Kategori Non Litigasi 2022",
        "desc":"Berdasarkan hasil survei yang dilakukan tahun ini posisi pertama kategori “Top 10 Largest Non-Litigation Practice Law Firm 2022” jatuh kepada Ginting & Reksodiputro in Association with Allen & Overy dengan 44 fee earners dalam bidang non-litigasi",
        "link":"https://www.hukumonline.com/berita/a/simak-deretan-law-firm-terbesar-kategori-non-litigasi-2022-lt631637fa18365/",
        "image":"https://images.hukumonline.com/frontend/lt631637fa18365/medium_lt631639b916d03.jpg"
    },
    {
        "title":"Ini Jajaran Pemenang Rising Star Law Firm 2022",
        "desc":"Berdasarkan hasil analisis terhadap fee earners 34 kantor hukum yang menjadi nominasi kategori ini, William Hendrik & Siregar Djojonegoro menduduki peringkat satu.",
        "link":"https://www.hukumonline.com/berita/a/ini-jajaran-pemenang-rising-star-law-firm-2022-lt63163c5d4a178/",
        "image":"https://images.hukumonline.com/frontend/lt63163c5d4a178/medium_lt63163da291c43.jpg"
    },
    {
        "title":"Melihat Deretan Law Firm Terbanyak Miliki Partner Wanita 2022",
        "desc":" Mochtar Karuwin Komar menduduki peringkat pertama sebagai kantor hukum yang memiliki Partner wanita terbanyak dengan jumlah 10 Partner Wanita dari total 13 Partner yang ada.",
        "link":"https://www.hukumonline.com/berita/a/melihat-deretan-law-firm-terbanyak-miliki-partner-wanita-2022-lt63163f9432fc2/",
        "image":"https://images.hukumonline.com/frontend/lt63163f9432fc2/medium_lt631640c7c40ba.jpg"
    },
]

const Wrapper = styled.div.attrs({
    className: 'max-w-5xl mx-auto flex flex-col  md:py-20 py-10',
})``
const WrapperHeader = styled.div.attrs({
    className: 'max-w-5xl mx-auto flex flex-col md:px-0 px-5 py-4 text-center',
})`
  & {
    h2 {
      ${tw`font-nunito text-header-3-xs md:text-header-3 font-extrabold text-neutral-500`}
    }

    p {
      ${tw`font-inter text-body-1-xs md:text-body-1 text-neutral-200 text-center py-4`}
    }
  }
`
const WrapperItem = styled.div.attrs({
    className: 'grid md:grid-cols-3 grid-cols-1 md:px-0 px-5 gap-8 py-4',
})``
const Item = styled.div.attrs({
    className: 'flex flex-col',
})`& {
  img {
    ${tw`rounded w-full`}
  }
  a{
    ${tw`no-underline hover:underline `}
  }

  h3 {
    ${tw`md:text-subtitle text-subtitle-xs font-bold font-nunito text-neutral-500 py-2`}
  }

  p {
    ${tw`font-inter md:text-body-2 text-body-2-xs text-neutral-200`}
  }

}`

const Insight = (props) => {
    return (<Wrapper>
            <WrapperHeader>
                <h2>THE INSIGHT</h2>
                <p>Deretan kantor hukum dan advokat terbaik dalam Capital Market Lawyers Ranking 2022.</p>
            </WrapperHeader>
            <WrapperItem>
                {contents && contents.map((item,index) =>(
                    <Item key={`insight-${index}`}>
                        <a href={item.link} target={"_blank"}  rel="noreferrer">
                            <div className={styles.imageContainer}>
                                <Image src={item.image} alt={"test"} layout="fill" className={styles.image}/>
                            </div>
                            <h3 className="">{item.title}</h3>
                            <p className="">{item.desc}</p>
                        </a>

                    </Item>
                ))}

            </WrapperItem>

        </Wrapper>);
};
export default Insight