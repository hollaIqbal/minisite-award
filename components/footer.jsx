// @flow
import * as React from 'react';
import {Copyright, FacebookLogo, InstagramLogo, LinkedinLogo, TwitterLogo, YoutubeLogo} from "phosphor-react";

export const Footer = (props) => {
    return (
        <div className="bg-black">
            <div className="max-w-5xl mx-auto flex md:flex-row flex-col items-center justify-center md:justify-between py-6">
                <div className="text-neutral-50 font-inter flex ">
                    <Copyright size={16} weight="bold" className="mr-3" />
                    <span className="text-neutral-50 font-inter text-caption">2022 Hak Cipta Milik Hukumonline.com</span>
                </div>
                <div className="text-neutral-50 font-inter flex order-first md:order-last mb-3 md:my-0">
                    <a href="https://www.instagram.com/hukum_online/" target="_blank" rel={"noreferrer"}>
                        <InstagramLogo size={19} weight="fill" className="mx-3"/>
                    </a>
                    <a href="https://www.facebook.com/hukumonlinecom/" target="_blank" rel={"noreferrer"}>
                        <FacebookLogo size={19} weight="fill" className="mx-3"/>
                    </a>
                    <a href="https://www.linkedin.com/company/hukumonline.com?originalSubdomain=id" target="_blank" rel={"noreferrer"}>
                        <LinkedinLogo size={19} weight="fill"  className={"mx-3"}/>
                    </a>
                    <a href="https://www.youtube.com/channel/UCUMVfBitV_9GbomOh5sgcTg" target="_blank" rel={"noreferrer"}>
                        <YoutubeLogo size={19} weight="fill"  className={"mx-3"}/>
                    </a>
                    <a href="https://twitter.com/hukumonline?lang=id" target="_blank" rel={"noreferrer"}>
                        <TwitterLogo size={19} weight="fill"  className={"mx-3"}/>
                    </a>
                </div>
            </div>
        </div>
    );
};
export default Footer