import {ArrowRight, Eye, X} from "phosphor-react";
import Image from "next/image";
import styles from "/styles/award.module.css"
import {useState} from "react";
const Award = (props) => {
    const [imageActive, setImageActive] = useState('');
    const gallery=[];
    for (let i = 1; i <= 12; i++) {
        gallery.push(
            <div
                className={`${styles.imageContainer} transition ease-in duration-300 transform hover:cursor-pointer`}
                onClick={()=>setImageActive(i.toString())}
                key={`gallery-${i}`}
            >
                <div className="absolute inset-0 z-10 flex transition duration-200 ease-in  bg-black opacity-0 hover:opacity-70">
                    <div className="absolute inset-0"></div>
                    <div className=" flex flex-col justify-center items-center mx-auto text-white z-10 self-center uppercase tracking-widest text-sm text-center">
                        <Eye size={18} weight="fill" />
                        <span>View Detail</span>
                    </div>
                </div>
                <Image src={`/img/gallery/${i.toString()}.webp`} layout="fill" className={styles.image} />
            </div>
        )

    }
    return (
        <div className={`bg-primary md:py-20 py-10 ${styles.awards}`} id={"awards"}>
            <div className={"max-w-5xl mx-auto"}>
                <div className="flex flex-col align-center text-center px-5">
                    <h3 className="text-header-3-xs md:text-header-3 text-neutral-50 font-nunito  font-extrabold mb-3">AWARDS NIGHT</h3>
                    <p className="text-body-1-xs md:text-subtitle text-subtitle-xs font-inter font-semibold text-neutral-50 py-5">Untuk pertama kalinya digelar ajang
                        Hukumonline’s Top 100 Indonesian Law Firms Awards Night.
                        Dengan harapan, Hukumonline bisa menjadi wadah mempererat tali silaturahmi dan menjalin
                        relasi antar sesama profesional hukum.</p>
                </div>
                <div className="grid md:grid-cols-4 grid-cols-2 gap-6 items-center  px-5 md:px-0">
                    {gallery}
                </div>
                <div className={"flex justify-center pt-8"}>
                    <a href="https://photos.app.goo.gl/RARbsn7T3fSfdj9C7" className={" font-nunito bg-secondary py-2 px-4 md:py-3 md:px-6 rounded text-body-3-xs md:text-body-3 font-bold text-neutral-500"} target={"_blank"} rel={"noreferrer"}>Selengkapnya  <ArrowRight  className={"ml-1 inline-block"} size={16} weight="bold" /></a>
                </div>
            </div>
            <div className={`fixed z-10 overflow-y-auto top-0 h-full transition transition-all duration-500 flex items-center justify-center w-full left-0 ${imageActive!=""?"":"hidden"}`} id="modal">
                <div
                    className="flex items-center justify-center min-height-100vh pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <div className="fixed inset-0 transition-opacity">
                        <div className="absolute inset-0 bg-gray-900 opacity-75"/>
                    </div>
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen">&#8203;</span>
                    <div
                        className="inline-block align-center bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                        role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                        <div >
                            <div className={styles.imageContainer}>
                                <div className="absolute inset-0 z-10 self-end ease-in p-3">
                                    <div className=" flex justify-end  text-white z-10 justify-items-end uppercase tracking-widest text-sm text-center">
                                        <a onClick={()=>setImageActive("")} className={"flex hover:text-secondary hover:cursor-pointer transition transition-all duration-200 "}>
                                            <X size={18} weight="fill" className={"mr-2"} />
                                            <span>Close</span>
                                        </a>
                                    </div>
                                </div>
                                <Image src={`/img/gallery/${imageActive}.webp`} layout="fill" className={styles.image} />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    );
}
export default Award;