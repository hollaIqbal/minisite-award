
import styled from "styled-components"
import tw from "twin.macro"
import {ArrowRight} from "phosphor-react";

const Wrapper = styled.div.attrs({
    className: "bg-primary-500",
})`
  & {
    div{
      ${tw`max-w-5xl mx-auto flex flex-col md:justify-center md:items-center items-start justify-start  md:py-20 py-10 px-5 `}
    }
    h3 {
      ${tw`font-nunito md:text-header-3 text-header-3-xs font-extrabold text-neutral-50`}
    }
    p{
      ${tw`font-inter text-body-1-xs md:text-body-1 text-neutral-50 text-left md:text-center py-4`}
    }
    a{
      ${tw` font-nunito bg-secondary py-2 px-4 md:py-3 md:px-6 rounded text-body-3-xs md:text-body-3 font-bold text-neutral-500`}
    }
    }
`

 const Desc = (props) => {
    return (
        <Wrapper>
            <div>
                <h3 className="font-nunito">TOP 100 INDONESIAN LAW FIRMS 2022</h3>
                <p >Sebagai bentuk apresiasi kepada ratusan kantor hukum di Indonesia, Hukumonline kembali menyelenggarakan pemeringkatan 100 kantor hukum Indonesia tahun 2022.
                    Pemeringkatan tidak hanya ditujukan kepada kantor hukum litigasi dan non-litigasi,
                    tetapi terdapat beberapa kategori baru lainnya dalam pemeringkatan tahun ini.
                    Temukan 100 kantor hukum terbaik yang masuk dalam pemeringkatan Top 100 Indonesian Law Firms 2022.
                </p>
                <a href="https://www.hukumonline.com/berita/a/ini-dia-kategori-juara-top-100-indonesian-law-firms-2022-lt630307bfe4e70/"  rel="noreferrer" target={"_blank"}>
                    Baca Artikel  <ArrowRight  className={"ml-1 inline-block"} size={18} weight="bold" />
                </a>
            </div>
        </Wrapper>
    );
};
export default Desc