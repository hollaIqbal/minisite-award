import Image from 'next/image';
import {CaretDown, List, X} from 'phosphor-react';
import logoHol from '../public/img/logo/hol-main.png';
import styled from "styled-components"
import tw, { screen, css } from 'twin.macro'
import {useEffect, useState} from "react";

const styles = [
    screen`xs`({ width: '180px', }),
]
export const ImageLogo = styled.img`
  @media (min-width: 768px) {
    width: 101px;
  }
  @media (max-width: 768px) {
   width: 80px;
  }
`

function Header(props) {
    const [activeClass, setActiveClass] = useState("");
    const [mobileMenu, seMobileMenu] = useState(false);
    useEffect(() => {
        let heightScreen =window.innerHeight;
        window.addEventListener("scroll", () => {
            if(window.scrollY > heightScreen){
                setActiveClass('bg-black opacity-80');
            }else{
                setActiveClass('');
            }
        });
    }, []);

  return (
    <div className={`fixed w-full transition-all duration-200 ${activeClass} ${mobileMenu && "bg-black opacity-80"} z-10`}>
        <header className="max-w-5xl mx-auto z-10">
            <nav className="px-2 sm:px-4 py-2.5 rounded">
                <div className="container flex flex-wrap justify-between items-center mx-auto">
                    <a href="https://www.hukumonline.com/" className="flex items-center">
                      <ImageLogo src={"/img/logo/hol-putih.svg"}/>
                    </a>
                    <button data-collapse-toggle="navbar-default" type="button"
                            className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                            aria-controls="navbar-default" aria-expanded="false" onClick={()=>seMobileMenu(!mobileMenu)}>
                        <span className="sr-only">Open main menu</span>
                        {mobileMenu ? (<X size={22} weight="fill" />) : <List size={22} weight="fill" />}
                    </button>
                    <div className="hidden w-full md:block md:w-auto" id="navbar-default">
                        <ul className="whitespace-nowrap no-scrollbar font-inter text-white font-semibold">
                            <li className="w-fit inline-block mx-1 p-2 px-3"><a href="https://ranking.hukumonline.com/" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">Home</a></li>
                            {/*<li className="w-fit inline-block mx-1 p-2 px-3"><a href="#featured" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">Featured</a></li>*/}
                            <li className="w-fit inline-block mx-1 p-2 px-3">
                                <a  href="#awards" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">
                <span>
                   Awards Night

                </span>
                                    {' '}
                                </a>
                            </li>
                            <li className="w-fit inline-block mx-1 p-2 px-3">
                                <a  href="#ranking" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">
                <span>
                  Rankings

                </span>
                                    {' '}
                                </a>
                            </li>
                            {/*<li className="w-fit inline-block mx-1 p-2 px-3"><a href="#" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">Practice Area</a></li>*/}
                            {/*<li className="w-fit inline-block mx-1 p-2 px-3"><a href="#" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">Previous</a></li>*/}
                            <li className="w-fit inline-block mx-1 p-2 px-3"><a href="https://www.hukumonline.com/" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3  border-transparent border-2">Hukumonline.com</a></li>
                        </ul>
                    </div>
                </div>
                <div>
                    <ul className={`${!mobileMenu && "hidden"} flex flex-col whitespace-nowrap no-scrollbar font-inter text-white font-semibold bg-black`}>
                        <li className="w-fit inline-block mx-1 py-2"><a href="https://ranking.hukumonline.com/" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3-xs  border-transparent border-2">Home</a></li>
                        <li className="w-fit inline-block mx-1 py-2">
                            <a href="#awards" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3-xs  border-transparent border-2">
                <span>
                  Awards Night
                </span>
                                {' '}
                            </a>
                        </li>
                        <li className="w-fit inline-block mx-1 py-2">
                            <a href="#ranking" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3-xs  border-transparent border-2">
                <span>
                  Rankings
                </span>
                                {' '}
                            </a>
                        </li>
                        {/*<li className="w-fit inline-block mx-1 py-2"><a href="#" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3-xs  border-transparent border-2">Practice Area</a></li>*/}
                        <li className="w-fit inline-block mx-1 py-2"><a href="https://www.hukumonline.com/" className="hover:border-b-secondary hover:transition-all duration-500 text-body-3-xs  border-transparent border-2">Hukumonline.com</a></li>
                    </ul>

                </div>
            </nav>
        </header>
    </div>
  );
}
export default Header;
