export const HLine = (props) => {
    return (
        <div className={"max-w-5xl mx-auto flex flex-col px-5 md:px-0 py-4 text-center"}>
            <hr/>
        </div>
    );
};
export default HLine;